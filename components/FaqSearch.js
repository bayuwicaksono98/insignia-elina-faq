import { SearchIcon } from '@heroicons/react/outline'
import React, { useState } from 'react'
import Fuse from 'fuse.js'
import FaqItem from './FaqItem'
import ReactMarkdown from 'react-markdown'
const qs = require('qs')

function FaqSearch() {
  const [searchValue, setSearchValue] = useState('')
  const [searchResults, setSearchResults] = useState({})
  async function searchInit(value) {
    setSearchValue(value)
    const options = {
      includeScore: true,
      keys: ['question', 'answer', 'category']
    }

    const query = qs.stringify({
      filters: {
        answer: {
          $containsi: value
        }
      },
    }, {
      encodeValuesOnly: true,
    });

    const list = await (await fetch(`https://elinaadm.daninaportal.id/frequently-asked-questions?_q=${value}`)).json()

    const fuse = new Fuse(list, options)
    const result = fuse.search(value)
    setSearchResults(list)
    console.log(list)
  }

  return (
    <>
      <div className="flex relative">
        <span className="rounded-l-md inline-flex shadow-lg items-center px-3 border-t bg-white border-l border-b border-gray-300 text-gray-500 text-sm">
          <SearchIcon
            className={'w-5 h-5'} />
        </span>
        <input type="text" className="rounded-r-lg shadow-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" onChange={e => { searchInit(e.target.value) }} name="search" placeholder="Search FAQ" />
      </div>
      <div className={'w-full'}>
        {searchValue && (
          <>
            <h1 className={'text-white text-center'}>Search Results</h1>
            <div className='justify-center flex flex-col my-4 bg-white md:w-2/4 shadow-lg mx-auto rounded-xl'>
              {Object.keys(searchResults).map((key, item) => (
                <FaqItem title={searchResults[item]['question']} key={searchResults[item]['id']}>
                  <ReactMarkdown>{searchResults[item]['answer']}</ReactMarkdown>
                </FaqItem>
              ))}
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default FaqSearch
