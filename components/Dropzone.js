import { CloudIcon, CloudUploadIcon, UploadIcon } from '@heroicons/react/outline';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: 'none',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};


export function UploadFiles(props) {
  const [files, setFiles] = useState([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      const files = acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))
      setFiles(files);
      props.setFiles(files)
    },
    onDropRejected: rejectedFiles => {
      alert('Error: File is not supported')
    },
    multiple: false
  });

  const thumbs = files.map(file => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <Image src={file.preview}
          className='rounded'
          width={'100%'}
          height={'100%'}
          style={img}
          objectFit='cover'
          alt={file.preview} />
      </div>
    </div>
  ));

  useEffect(() => () => {
    // Make sure to revoke the data uris to avoid memory leaks
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);

  return (
    <section className="container bg-purple-200 hover:ring-2 ring-purple-500 p-10 rounded-2xl text-purple-500">
      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <CloudUploadIcon className='w-8 h-8' />
        <p>Drag some files here, or click to select files</p>
      </div>
      <aside style={thumbsContainer}>
        {thumbs}
      </aside>
    </section>
  );
}

export default UploadFiles;