import { TicketIcon } from '@heroicons/react/outline';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import createTicket from '../../pages/api/ticket';
import UploadFiles from '../Dropzone'
import Modal from '../Modal';

function SupportTicket() {

  const [supportTicket, setSupportTicket] = useState({})

  const [files, setFiles] = useState({})

  const [categories, setCategories] = useState([])

  useEffect(() => {
    const categoriesResponse = async () => {
      const result = await axios('https://elinaadm.daninaportal.id/support-ticket-categories')
      setCategories(result.data)
    }
    categoriesResponse()
  }, [])

  // console.log(categories)
  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      createTicket({ ...supportTicket, files: files[0] }).then((response) => { alert(`${!response.message ? 'Successfully Submitted' : response.message}`) }
      )
    } catch (error) {
      alert(error)
      console.log(error)
    }
  }

  return (
    <form onSubmit={handleSubmit} className="flex w-full max-w-sm space-x-3">
      <div className="w-full max-w-2xl m-auto mt-2 bg-gray-100 p-10 rounded-2xl">
        <div className="grid max-w-xl grid-cols-2 gap-4 m-auto">
          <div className="col-span-2 lg:col-span-1">
            <div className=" relative ">
              <input required type="text" onChange={e => setSupportTicket({ ...supportTicket, name: e.target.value })} name='name' autoComplete='name' className="rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Name" />
            </div>
          </div>
          <div className="col-span-2 lg:col-span-1">
            <div className=" relative ">
              <input required type="email" name='email' onChange={e => setSupportTicket({ ...supportTicket, email: e.target.value })} autoComplete='email' className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Email" />
            </div>
          </div>
          <div className="col-span-2">
            <label className={'text-gray-700'} htmlFor='phone'>
              <input required type="tel" name='phone' onChange={e => setSupportTicket({ ...supportTicket, phone: e.target.value })} className="rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder='Phone Number' />
            </label>
          </div>
          <div className='col-span-2'>
            <label className={'text-gray-700'} htmlFor='elina'>
              <input required type="text" name='elina' onChange={e => setSupportTicket({ ...supportTicket, elina: e.target.value })} className="rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder='Elina Code' />
            </label>
          </div>
          <div className='col-span-2'>
            <label className="text-gray-500 inline-flex flex-col gap-4" htmlFor="file">
              Upload File
              <UploadFiles setFiles={setFiles} />
            </label>
          </div>
          <div className="col-span-2">
            <label className="text-gray-700" htmlFor="category">
              <select name="category" defaultValue={''} id="category" className='rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent' onChange={e => setSupportTicket({ ...supportTicket, category: e.target.value })}>
                <option value="" hidden>Select Category</option>
                {categories.map((item) => {
                  return (
                    <option key={item.id} value={item.id}>{item.name}</option>
                  )
                })}
              </select>
            </label>
          </div>
          <div className="col-span-2">
            <label className="text-gray-700" htmlFor="details">
              <textarea required name={'details'} onChange={e => setSupportTicket({ ...supportTicket, details: e.target.value })} className="flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 rounded-lg text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" id="comment" placeholder="Enter your comment" rows="5" cols="40">
              </textarea>
            </label>
          </div>
          <div className="col-span-2 text-right">
            <button type="submit" className="py-2 px-4  bg-purple-600 hover:bg-purple-700 focus:ring-purple-500 focus:ring-offset-purple-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
              Send
            </button>
          </div>
        </div>
      </div>
    </form>
  )
}

export default SupportTicket
