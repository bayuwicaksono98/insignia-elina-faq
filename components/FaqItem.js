import { Disclosure, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/outline'
import React from 'react'

function FaqItem(props) {
  return (
    <Disclosure as={'div'} className={'px-4 py-2 flex flex-col gap-4'}>{({ open }) => (
      <>
        <Disclosure.Button className="flex justify-between w-full px-4 py-2 text-sm font-medium text-left text-gray-500 hover:text-purple-900 hover:border-purple-200 border-white border-2 rounded-lg hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
          <span className='w-4/5'>{props.title}</span> <ChevronDownIcon className={`w-5 h-5 transition-transform ${open ? "transform rotate-180" : ''}`} />
        </Disclosure.Button>
        <Transition
          enter="transition duration-100 ease-out"
          enterFrom="transform scale-95 opacity-0"
          enterTo="transform scale-100 opacity-100"
          leave="transition duration-75 ease-out"
          leaveFrom="transform scale-100 opacity-100"
          leaveTo="transform scale-95 opacity-0"
        >
          <Disclosure.Panel className="text-gray-500 border-purple-200 border-2 rounded-xl px-2 pt-2 pb-2 text-sm leading-loose">
            {props.children}
          </Disclosure.Panel>
        </Transition>
      </>
    )}
    </Disclosure>
  )
}

export default FaqItem
