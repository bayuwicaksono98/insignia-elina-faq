import React, { useState } from 'react'
import { SearchIcon, ViewListIcon } from '@heroicons/react/outline'
import FaqItem from '../components/FaqItem'
import { useRouter } from 'next/router'
import axios from 'axios'
import ReactMarkdown from 'react-markdown'
import Header from '../components/Header'
import _, { forEach } from 'lodash'
import { Switch, Tab } from '@headlessui/react'
import FaqSearch from '../components/FaqSearch'
import qs from 'qs'
import Modal from '../components/Modal'
import UploadFiles from '../components/Dropzone'
import createTicket from './api/ticket'
import SupportTicket from '../components/forms/SupportTicket'

function Faq({ faqs, category }) {
  const router = useRouter()
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

  const [enabled, setEnabled] = useState(false)

  if (router.isFallback) {
    return <div>Loading...</div>
  }
  return (
    <>
      <Header />
      <main className={'relative bg-gradient-to-r w-max-md px-2 py-16 sm:px-0 from-indigo-500 to-purple-700 gap-4 text-gray-600 m-auto w-full min-h-screen'}>
        <div className={"m-auto inline-flex flex-col items-center w-full p-4 gap-4"}>
          <h1 className={'text-center text-white text-shadow-lg text-3xl font-bold'}>How can we help you today?
          </h1>

          <FaqSearch />
        </div>
        <Modal title={'Submit Ticket'} openButtonValue={'Submit a Ticket'} closeButtonValue={'Dismiss'} >
          <SupportTicket />
        </Modal>
        <Switch.Group as={'div'} className={'flex items-center md:w-2/4 justify-end my-4 mx-auto'}>
          <div className={'bg-white p-2 rounded-xl shadow-md flex items-center'}>
            <Switch.Label className={'mr-4 text-gray-500'}>
              <ViewListIcon className='w-5 h-5' />
            </Switch.Label>
            <Switch
              checked={enabled}
              onChange={setEnabled}
              className={`${enabled ? 'bg-purple-600' : 'bg-gray-200'
                } relative inline-flex items-center h-6 rounded-full w-11`}
            >
              <span className="sr-only">Tab View</span>
              <span
                className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                  } inline-block w-4 h-4 transform transition-transform bg-white rounded-full`}
              />
            </Switch>

          </div>

        </Switch.Group>
        {enabled ? (
          <Tab.Group>
            <Tab.List className={'flex p-1 md:w-2/4 mx-auto space-x-1 bg-purple-900/20 rounded-xl'}>
              {Object.keys(category).map((item) => (
                <Tab
                  key={item}
                  className={({ selected }) =>
                    classNames(
                      'w-full py-2.5 text-sm leading-5 font-medium text-purple-700 rounded-lg',
                      'focus:outline-none focus:ring-2 ring-offset-2 ring-offset-purple-400 ring-white ring-opacity-60',
                      selected
                        ? 'bg-white shadow'
                        : 'text-purple-100 hover:bg-white/[0.12] hover:text-white'
                    )
                  }
                >
                  {item}
                </Tab>
              ))}
            </Tab.List>
            <div className={'justify-center flex flex-col my-4 bg-white md:w-2/4 shadow-lg mx-auto rounded-xl'}>
              <Tab.Panels>
                {/* <Tab.Panel>
                {faqs.map((item) => (
                  <FaqItem question={item.question} key={item.id} answer={<ReactMarkdown>{item.answer}</ReactMarkdown>} />
                ))}
              </Tab.Panel> */}
                {Object.keys(category).map((key, item) => (
                  <Tab.Panel key={item}>
                    {category[key].map((item) => (
                      <FaqItem title={item.question} key={item.id}>
                        <ReactMarkdown>{item.answer}</ReactMarkdown>
                      </FaqItem>
                    ))}
                  </Tab.Panel>
                ))}
              </Tab.Panels>
            </div>
          </Tab.Group>

        ) : (
          <div className={'md:w-2/4 mx-auto bg-white rounded-2xl'}>
            {Object.keys(category).map((key, item) => (
              <>
                {/* <h1>{item}</h1> */}
                <FaqItem title={key} key={item}>
                  {category[key].map((item) => (
                    <>
                      <FaqItem title={item.question} key={item.id}>
                        <ReactMarkdown>{item.answer}</ReactMarkdown>
                      </FaqItem>
                    </>
                  ))}
                </FaqItem>
              </>
            ))}

          </div>

        )}
      </main>
    </>
  )
}

export async function getServerSideProps({ params }) {
  process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
  const faq = await (await fetch(`${process.env.BACKEND_URL}/frequently-asked-questions?_sort=order:ASC`)).json()
  const category = _.groupBy(faq, 'category')

  const responses = await Promise.all([faq, category])
  // console.log({ category })

  return {
    props: { faqs: responses[0], category: responses[1] },
  }
}

export default Faq