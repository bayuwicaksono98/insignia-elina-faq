import axios from 'axios';
import { assignIn } from 'lodash';
import React from 'react'
import upload from './upload';

export default async function createTicket(data) {
  const formData = new FormData();

  // formData.append('name', data.name)
  // formData.append('email', data.email)
  // formData.append('phone', data.phone)
  // formData.append('elina', data.elina)
  // formData.append('details', data.details)
  if (data.files !== null) {
    formData.append('files.files', data.files)
  }
  // formData.append('data', JSON.stringify({
  //   name: data.name,
  //   email: data.email,
  //   phone: data.phone,
  //   // code: data.elina,
  //   support_ticket_category: data.category,
  //   details: data.details
  // }))
  // formData.append('ticket_type', data.ticket_type)

  const newData = data
  try {
    const getElina = await axios.get(`https://elinaadm.daninaportal.id/elinas?code=${data.elina}`)
    console.log({ getElina })
    if (getElina.data.length !== 0) {
      const elinaCode = getElina.data[0]?.id;
      console.log({ elinaCode })
      formData.append('data', JSON.stringify({
        name: data.name,
        email: data.email,
        phone: data.phone,
        support_ticket_category: data.category,
        details: data.details,
        code: elinaCode
      }))
      console.log(formData.get('data'))
      const create = await axios.post(`https://elinaadm.daninaportal.id/support-tickets`, formData)
      return create
    }
    return ({
      status: 404,
      message: "Elina code not found"
    })
  } catch (error) {
    if (error.response) {
      return error.response.data
    }
    else return error
  }

}
