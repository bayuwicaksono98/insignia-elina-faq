import axios from 'axios'
import React from 'react'

async function upload(data) {
  const formData = new FormData();
  formData.append('files', data)
  await axios.post(`https://elinaadm.daninaportal.id/upload`, formData).then((response) => {
    console.log(response.data)
    return response.data[0]
  }).catch(error => alert(error))
}

export default upload
